import respon.verbal.elsaspeech as elsa
from time import ctime

def introduction(voice_data):
    if 'what is your name' in voice_data or "what's your name" in voice_data :
        elsa.elsa_speech ("my name is Elsa")
    if 'who am i' in voice_data or "do you know me" in voice_data :
        elsa.elsa_speech ("you are my master")
    if 'i love you' in voice_data:
        elsa.elsa_speech ("i love you too")
    if 'elsa' in voice_data:
        elsa.elsa_speech ("Yes i am, why?")
    if  'hi' in voice_data:
        elsa.elsa_speech ("Hai Widy")
    if 'what time is it' in voice_data:
        elsa.elsa_speech (ctime())
    