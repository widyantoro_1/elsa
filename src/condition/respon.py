import respon.verbal.elsaspeech as elsa
import condition.introduction.intro as int



def respond(voice_data):
    if 'exit' in voice_data or 'turn off' in voice_data or 'shutdown' in voice_data:
        elsa.elsa_speech ("Okey, i will turn off, good bye, see you next time")
        exit()
    else:
        int.introduction(voice_data)