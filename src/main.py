import record_audio as record
import respon.verbal.elsaspeech as elsa
import condition.respon as respon
import time



if __name__ == '__main__':
    time.sleep(0.01) 
    elsa.elsa_speech("How can I help you?")
    while 1:
        voice_data = record.record_audio()
        respon.respond(voice_data.lower())
