from gtts import gTTS
import os
import playsound
import random

def elsa_speech(audio_string):
    try:
        print(audio_string)
        tts = gTTS(text= audio_string, lang='en')
        r = random.randint(1, 100000)
        audio_file = 'audio' + str(r) +'.mp3'
        tts.save(audio_file)
        playsound.playsound(audio_file)
        print(audio_string)
        os.remove(audio_file)
    except:
        print("error")