
import respon.verbal.elsaspeech as elsa
import speech_recognition as sr
import time
r = sr.Recognizer()
def record_audio(ask = False) :
    with sr.Microphone() as source:
        if ask:
            elsa.elsa_speech(ask)
        audio = r.listen(source)
        voice_data = ""
        try: 
            voice_data = r.recognize_google(audio,language='id-ID')
            print(voice_data)
        except sr.UnknownValueError:
            elsa.elsa_speech("Sorry, i did not get that")
        except sr.RequestError:
            elsa.elsa_speech("Sorry, my speech service is down")
        return voice_data 


# if __name__ == '__main__':
    
#     time.sleep(0.01) 
#     while 1:
#         record_audio